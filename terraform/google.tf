terraform {
  backend "gcs" {
    bucket = "gke-project-terraform-simple-node-app"
    prefix = "terraform"
    credentials = "cred.json"
  }
}

provider "google" {
  credentials = "${file("cred.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}

