# DevOps Challenge

A simple Node.js Hello World API.

## Running Locally

To run locally with docker:

```bash
docker build -t hello-node .
docker run --rm --name hello-node -p 3000:3000 hello-node
```

or you can also use docker-compose:

```bash
docker-compose up
```

Your app should now be running on localhost:3000.

## Helm

To install the chart with the release name `my-release`:

```bash
helm upgrade --install my-release hello-chart
```


### Configuration options

The following table lists the configurable parameters of the app chart and their default values

| Parameter                                | Description                                                                               | Default                                     |
| -----------------------------            | ------------------------------------                                                      | ------------------------------------------- |
| `replicaCount`                         | Number of replicas to deploy                                                                      | `1`         |
| `image.repository`                       | The image repository to pull from                                                         | `lucianocarranza/simple-node-app`                             |
| `image.tag`                              | The image tag to pull                                                                     | `latest`                                    |
| `image.pullPolicy`                       | Image pull policy                                                                         | `IfNotPresent`                              |
| `container.port`                      | Container TCP port                                                                       | `3000`                                       |
| `nameOverride`                           | Override name of app                                                                      | `nil`                                       |
| `fullnameOverride`                       | Override full name of app                                                                 | `nil`                                       |
| `service.type`                    | Kubernetes service type                    | `LoadBalancer`                                            |
| `service.port`                    | Service port                          | `3000`                                                      |
| `ingress.enabled`                 | Enable ingress controller resource         | `false`                                                   |
| `ingress.http.paths`           | Path within the url structure              | `/`                                                       |
| `ingress.annotations`    | Annotations for this host's     ingress record | `{}`                                                      |


## Terraform

Create an environment .tfvars file, and complete the mandatory values.

For example, if you wish to create the development cluster, complete the following values in a new file called dev.tfvars inside the terraform folder:
```bash
# Mandatory
project = "GCP_PROJECT_ID"
region = "REGION" # Ex: us-east4
general_purpose_machine_type = "MACHINE_TYPE" # Ex: n1-standard-1

# Optional (if not declared uses default values)
general_purpose_min_node_count = 1 # Default 1
general_purpose_max_node_count = 1 # Default 5
```

Also you need a service account key with access to Google Cloud Storage and GKE.

Save the key inside the terraform folder in a new file and name it cred.json.

To create the development cluster:

```bash
terraform init
terraform apply -var-file=dev.tfvars
```

<script id="asciicast-tTR9UpdiApxfAUrR74BOitVUk" src="https://asciinema.org/a/tTR9UpdiApxfAUrR74BOitVUk.js" async></script>
[![asciicast](https://asciinema.org/a/tTR9UpdiApxfAUrR74BOitVUk.svg)](https://asciinema.org/a/tTR9UpdiApxfAUrR74BOitVUk)
