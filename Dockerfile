FROM node:12.5.0-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .
RUN npm install --production

# Bundle app source
COPY . .

# Config port
EXPOSE 3000

CMD [ "npm", "start" ]

USER node
